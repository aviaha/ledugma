<?php

use yii\db\Migration;

/**
 * Class m180623_180658_init_rbac
 */
class m180623_180658_init_rbac extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $auth = Yii::$app->authManager;

 ///////////הגדרת התפקידים /////////////

    $admin = $auth->createRole('admin');
      $auth->add($admin);
              
      $editor = $auth->createRole('editor');
      $auth->add($editor);

      $author = $auth->createRole('author');
      $auth->add($author);

////////////////הגדרת ירושות///////////
      $auth->addChild($admin, $editor);
      $auth->addChild($editor, $author);

 //////////////// הגדרת ההרשאות //////////////////
      $manageUsers = $auth->createPermission('manageUsers');//אדמין
      $auth->add($manageUsers);

     $publishPost = $auth->createPermission('publishPost');//אדיטור
      $auth->add($publishPost); 

      $cteatePost = $auth->createPermission('cteatePost');//אוטור
      $auth->add($cteatePost);   

      $vieweOwnPost = $auth->createPermission('vieweOwnPost'); ////צפיה בפרטים של עצמך בלבד- תנאי מורכב יותר 
      $rule = new \app\rbac\AuthorRule;
      $auth->add($rule);
      $vieweOwnPost->ruleName = $rule->name;                
      $auth->add($vieweOwnPost);                 
        

//////////////////////קביעה מי יכול ליישם איזה חוק
      $auth->addChild($admin, $manageUsers);
      $auth->addChild($editor, $publishPost);
      $auth->addChild($author, $cteatePost); 
      $auth->addChild($author, $vieweOwnPost); 
      $auth->addChild($vieweOwnPost, $cteatePost);   

////////////////////// הגדרת המשתמשים הרשומים המערכת ()
    $auth->assign($admin,1);
    $auth->assign($editor,2);
    $auth->assign($author,3);
    }

    

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180623_180658_init_rbac cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180623_180658_init_rbac cannot be reverted.\n";

        return false;
    }
    */
}
