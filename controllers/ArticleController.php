<?php

namespace app\controllers;

use Yii;
use app\models\Article;
use app\models\ArticleSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Html;



/**
 * ArticleController implements the CRUD actions for Article model.
 */
class ArticleController extends Controller // יורשים מהמחלקה הבסיסית קונטרולר
{
    /**
     * @inheritdoc
     */
    public function behaviors() //
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'], // אם רוצים למחוק חייבים לעשות פוסט
                ],
            ],
        ];
    }

    /**
     * Lists all Article models.
     * @return mixed
     */
    public function actionIndex() //מביא אותך לאינדקס אם לא תרשום כלום אחרי הארטיקל ביו-אר-אל
    {
        $searchModel = new ArticleSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams); //-לסארצ מודל יש פונקציה השם סארצ שנשלח לה את הפרמטרים שבסוגרים-מה שאחרי הסוגריים ביו אר אל

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Article model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id) // מציג רשומה יחידה
    {
        $model = $this->findModel($id); // מביא את המאמר כאובייקט
        $tagModels = $model->tags; // מביא מתוך האובייקט שבמודל את התכונה טאג ומכניס לטאגמודל
        // נשלח לוי את הקישורים כקישורים, לחץ נחלץ אותם כאן
        $tags = ''; //טאג מסוג סטרינג
        foreach ($tagModels as $tag ){
        $tagLink = Html::a($tag->name,['article/index','ArticleSearch[tag]'=> $tag->name]); // ,שימוש בפונקציה שבונה לינק- בסוגריים מה נקבל, [לאן נלך, פרמטר שצריך לקרוא לו-טאג לא פרמטר שמור, חץ, 
        $tags .= ",".$tagLink; // שירשור הלינקים
        }

        $tags = substr($tags,1); //מוחק את הפסיק הראשון

        return $this->render('view', [
            'model' => $this->findModel($id),
            'tags' => $tags // הטאג בלי הדולג הופך לטאג עם דולר בוויו
        ]);
    }

    /**
     * Creates a new Article model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() //יצירה
    {

        $model = new Article(); //אובייקט ריק מסוג ארטיקל

        if ($model->load(Yii::$app->request->post()) && $model->save()) { // אם הגיעו נתונים מהבקשה (אפ ריקווסט) בשיטת פוסט וגם הצלחנו לשמור את הנתונים אז תכנס. אם לא נעמוד בחוקי הוליזציה לא יעבוד כי הפונקציה סייב (חלק מאקטיברקורד) לא עבדה!!
            return $this->redirect(['view', 'id' => $model->id]); //להעביר לוויו שהאיי די הוא המשתנה מודל
        }

        return $this->render('create', [ // אם לא נכנסנו לאיפ
            'model' => $model, //נשארים בטופס והערכים נשמרים
        ]);
    }

    /**
     * Updates an existing Article model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id); // חייב לעדכן מישהו ספציפי-מקבל איידי
        if (\Yii::$app->user->can('updateArticle',['article' => $model])) // מוסרים את המאמר לחוק
        {
             if ($model->load(Yii::$app->request->post()) && $model->save()) 
             {
                 return $this->redirect(['view', 'id' => $model->id]);
             }
            return $this->render('update', [
            'model' => $model,
            ]);
         }
         throw new ForbiddenHttpException('sorry this is not your article');
    }

    /**
     * Deletes an existing Article model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete(); //פעולת מחיקה

        return $this->redirect(['index']); // חוזרים לרשימת האובייקטים
    }

    /**
     * Finds the Article model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Article the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Article::findOne($id)) !== null) { // מחפש איי די שקיבלנו ביו-אר-אל ומכניס לנאל
            return $model; //מחזיר את האיי די שננמצא בתור מודל
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    

  

}
