<?php

/* @var $this yii\web\View */
use yii\grid\GridView;
use yii\widgets\DetailView;

    echo DetailView::widget([
    'model' => $model,
    'attributes' => [
        'id',
        'body',
        'title',
        'category_id',
    ],
]) ?>


    <code><?= __FILE__ ?></code>
</div>