<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\article1 */

$this->title = 'Create Article1';
$this->params['breadcrumbs'][] = ['label' => 'Article1s', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="article1-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
