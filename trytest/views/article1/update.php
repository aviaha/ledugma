<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\article1 */

$this->title = 'Update Article1: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Article1s', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="article1-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
