<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "product1".
 *
 * @property int $id
 * @property string $title
 * @property string $body
 * @property int $category_id
 */
class Product1 extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product1';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['body'], 'string'],
            [['category_id'], 'integer'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'body' => 'Body',
            'category_id' => 'Category ID',
        ];
    }
}
