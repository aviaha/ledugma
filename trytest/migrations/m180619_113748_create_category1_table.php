<?php

use yii\db\Migration;

/**
 * Handles the creation of table `category1`.
 */
class m180619_113748_create_category1_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
    
        $this->createTable('category1', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'created_at' => $this->timestamp(),
            'created_by' => $this->timestamp(),
            'updated_at' => $this->integer(),
            'updated_by' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('category1');
    }
}
