<?php

use yii\db\Migration;

/**
 * Handles the creation of table `article1`.
 */
class m180618_182434_create_article1_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
            $this->createTable('article1', [
                'id' => $this->primaryKey(),
                'title' => $this->string(),
                'descriptin' => $this->string(),
                'body' => $this->text(),
                'author_id' => $this->integer(),
                'editor_id' => $this->integer(),
                'category_id' => $this->integer(),
                'created_at' => $this->timestamp(),
                'updated_at' => $this->timestamp(),
                'created_by' => $this->integer(),
                'updated_by' => $this->integer(),
            ]);
     
    
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('article1');
    }
}
