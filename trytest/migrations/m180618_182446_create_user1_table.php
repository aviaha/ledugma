<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user1`.
 */
class m180618_182446_create_user1_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {   $this->createTable('user1', [
        'id' => $this->primaryKey(),
        'name'=> $this->string(),
        'email' => $this->string(),
        'username' => $this->string()->unique(),
        'auth_key' => $this->string(),
        'password' => $this->string(),
        'created_at' => $this->timestamp(),
        'updated_at' => $this->timestamp(),
        'created_by' => $this->integer(),
        'updated_by' => $this->integer(),
    ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('user1');
    }
}               

$this->dropForeignKey(
    'fk-product-author_id',
    'user1'
);
$this->dropForeignKey(
    'fk-product-editor_id',
    'user1'
); 
$this->dropForeignKey(
    'fk-product-created_by',
    'user1'
); 
$this->dropForeignKey(
    'fk-product-updated_by',
    'user1'
);          
